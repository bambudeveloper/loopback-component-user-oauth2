module.exports = function(OAuthAccessToken) {

OAuthAccessToken.observe('before delete', function(ctx, next) {
  console.log('Going to delete %s matching %j',
    ctx.Model.pluralModelName,
    ctx.where);
  console.log('Full ctx : ',ctx);
  next();
});

}